# Generated by Django 3.0.4 on 2020-03-21 13:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='StoreCapacity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
                ('total_capacity', models.IntegerField(default=0)),
                ('capacity_for_queue', models.IntegerField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='store',
            name='zip_code',
        ),
        migrations.AlterField(
            model_name='store',
            name='lat',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True),
        ),
        migrations.AlterField(
            model_name='store',
            name='long',
            field=models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True),
        ),
        migrations.CreateModel(
            name='Timeslot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('identifier', models.CharField(max_length=100, unique=True)),
                ('store_capacity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cms.StoreCapacity')),
            ],
        ),
        migrations.AddField(
            model_name='storecapacity',
            name='store',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cms.Store'),
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=100)),
                ('housenumber', models.CharField(max_length=100)),
                ('zip_code', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('country', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cms.Country')),
            ],
        ),
        migrations.AddField(
            model_name='store',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cms.Address'),
        ),
    ]
