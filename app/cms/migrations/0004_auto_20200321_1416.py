# Generated by Django 3.0.4 on 2020-03-21 14:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0003_auto_20200321_1358'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Timeslot',
            new_name='Booking',
        ),
    ]
