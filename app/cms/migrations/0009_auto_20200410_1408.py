# Generated by Django 3.0.4 on 2020-04-10 14:08

import cms.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0008_auto_20200321_2055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='identifier',
            field=models.CharField(editable=False, max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='store_capacity',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cms.StoreCapacity', validators=[cms.validators.validate_booking_fits_in_store_capacity]),
        ),
    ]
