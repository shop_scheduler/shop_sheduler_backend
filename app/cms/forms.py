from django import forms
from .models import Store

class BatchGenerateStoreCapacities(forms.Form):
    start_date = forms.CharField()
    start_time = forms.CharField()
    store = forms.ModelChoiceField(queryset=Store.objects.all(), help_text="Der Laden, für den Kapazitäten generiert werden.")
    duration = forms.IntegerField(help_text="Wie lange dauert eine Kapazität?")
    amount = forms.IntegerField(help_text="Wieviele Kapazitäten sollen angelegt werden?")
    total_capacity = forms.IntegerField(help_text="Gesamtkapazität des Ladens")
    reserved_capacity = forms.IntegerField(help_text="Kapazität reserviert für normale Kunden")