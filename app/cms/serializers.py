from .models import Store, Address, Country, StoreCapacity, Booking, StoreType
from rest_framework import serializers


class CountrySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    def get_id(self, country):
        return country.id

    name = serializers.CharField(max_length=100)
    class Meta:
        model = Country
        fields = ["id", "name"]


class AddressSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    def get_id(self, address):
        return address.id

    street = serializers.CharField(max_length=100)
    housenumber = serializers.CharField(max_length=100)
    zip_code = serializers.CharField(max_length=100)
    city = serializers.CharField(max_length=100)

    country = CountrySerializer()


    def get_country(self, address):
        return address.country


    class Meta:
        model = Address
        fields = ["id", "street", "housenumber", "zip_code", "city", "country"]



class StoreTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = StoreType
        fields = ["id", "name"]


class StoreSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    def get_id(self, store):
        return store.id

    address = AddressSerializer()
    type = StoreTypeSerializer()

    def get_address(self, store):
        return store.address

    class Meta:
        model = Store
        fields = ["id",'name','type', 'description', 'address', 'lat', 'long']



class StoreCapacitySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    def get_id(self, storecapacity):
        return storecapacity.id


    start = serializers.DateTimeField()
    end = serializers.DateTimeField()
    total_capacity = serializers.IntegerField()
    reserved_capacity = serializers.IntegerField()
    available_capacity = serializers.IntegerField()
    store = StoreSerializer()

    def get_available_capacity(self, storecapacity):
        return self.available_capacity


    class Meta:
        model = StoreCapacity
        fields = ["id", "store", "start", "end", "total_capacity", "reserved_capacity", "available_capacity"]



class BookingSerializer(serializers.Serializer):

    id = serializers.IntegerField()
    def get_id(self, booking):
        return booking.id
    identifier = serializers.CharField(max_length=100)
    store_capacity = StoreCapacitySerializer()

    class Meta:
        model = Booking
        fields = ["id", "identifier", "store_capacity"]
