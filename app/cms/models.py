from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q, F
from .validators import validate_booking_fits_in_store_capacity

class StoreType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Address(models.Model):
    street = models.CharField(max_length=100)
    housenumber = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.street + " " + self.housenumber + ", " + self.zip_code + " " + self.city



class Store(models.Model):
    name = models.CharField(max_length=999)
    description = models.TextField(max_length=999, null=True, blank=True)
    address = models.ForeignKey(Address, on_delete=models.SET_NULL, null=True, blank=True)
    type = models.ForeignKey(StoreType, on_delete=models.SET_NULL, null=True, blank=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    long = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)

    def __str__(self):
        return_value = self.name
        if self.address:
            return_value = self.name + " " + self.address.zip_code
        return return_value


class StoreCapacity(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField()
    total_capacity = models.IntegerField(default=0)
    reserved_capacity = models.IntegerField(default=0)

    def available_capacity(self):
        booked_timeslot_amout = Booking.objects.filter(store_capacity=self).count()
        return self.total_capacity - self.reserved_capacity - booked_timeslot_amout



    def __str__(self):
        return self.store.name + ": (" + str(self.available_capacity()) + ") " + self.start.strftime("%d.%m.%y %H:%M")+ " - " + self.end.strftime("%d.%m.%y %H:%M")


class Booking(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    identifier = models.CharField(editable=False, unique=True, max_length=100)
    store_capacity = models.ForeignKey(StoreCapacity, on_delete=models.CASCADE, validators=[validate_booking_fits_in_store_capacity,])

    class Meta:
        unique_together = ["user", "store_capacity"]
