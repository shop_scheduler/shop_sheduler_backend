from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core import serializers
from django.utils.crypto import get_random_string
from django.urls import reverse
import datetime as dt


from .forms import BatchGenerateStoreCapacities
from .serializers import BookingSerializer, StoreSerializer

from .models import Store, Address, Country, StoreCapacity, Booking, StoreType
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import StoreSerializer, AddressSerializer, CountrySerializer, StoreCapacitySerializer, BookingSerializer, StoreTypeSerializer
import json

class StoreTypeViewSet(viewsets.ModelViewSet):
    queryset = StoreType.objects.all()
    serializer_class = StoreTypeSerializer
    http_method_names = ['get', 'head']

class StoreViewSet(viewsets.ModelViewSet):
    serializer_class = StoreSerializer
    http_method_names = ['get', 'head']

    def get_queryset(self):
        queryset = Store.objects.all().order_by('address__zip_code')
        typeid = self.request.query_params.get('typeid', None)
        if typeid is not None:
            queryset = queryset.filter(type__pk=typeid)
        return queryset

class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    http_method_names = ['get', 'head']


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    http_method_names = ['get', 'head']


class StoreCapacityViewSet(viewsets.ModelViewSet):
    queryset = StoreCapacity.objects.all()
    serializer_class = StoreCapacitySerializer
    http_method_names = ['get', 'head']

    def get_queryset(self):
        queryset = StoreCapacity.objects.all()
        storeid = self.request.query_params.get('storeid', None)
        if storeid is not None:
            queryset = queryset.filter(store__pk=storeid)
        queryset.filter()
        return queryset


class BookingViewSet(viewsets.ModelViewSet):
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    http_method_names = ['get', 'head']


@csrf_exempt
@require_http_methods(["POST"])
def place_booking(request, storecapacityid):
    storecapacity = StoreCapacity.objects.filter(pk=storecapacityid).first()
    if storecapacity.available_capacity() > 0:
        booking = Booking(identifier=get_random_string(length=6, allowed_chars='1234567890'), store_capacity=storecapacity)
        booking.save()
        data = BookingSerializer(booking).data
    else:
        data = {'Success': 'False', 'Error': 'Storecapacity is not sufficient.'}
    return JsonResponse(data, content_type='application/json')


@csrf_exempt
@require_http_methods(["POST"])
def validate_booking(request, identifier):
    result = {"valid": 'False'}
    booking = Booking.objects.filter(identifier=identifier).first()
    if booking:
        store = booking.store_capacity.store
        storedata = StoreSerializer(store).data
        result["valid"] = 'True'
        result["start"] = booking.store_capacity.start
        result["end"] = booking.store_capacity.end
        result["store"] = storedata

    print(result)
    return JsonResponse(result, content_type='application/json')


@login_required(login_url='/admin/login/')
def batch_generate_store_capacities(request):

    # If this is a POST request then process the Form data
    form = BatchGenerateStoreCapacities(request.POST)
    context = {
        'form': form,
    }

    print(request.method)
    print(request.method == 'POST')

    if request.method == 'POST':

        # Create a form instance and populate it with data from the request (binding):

        # Check if the form is valid:
        print(form.is_valid())
        if form.is_valid():
            print(form.cleaned_data)

            origin_start_datetime = dt.datetime.strptime(form.cleaned_data["start_date"] + "-" + form.cleaned_data["start_time"], "%d.%m.%Y-%H:%M")
            duration = dt.timedelta(minutes=form.cleaned_data["duration"])
            for i in range(0, int(form.cleaned_data['amount'])):
                start_datetime = origin_start_datetime + i*duration
                end_datetime = origin_start_datetime + (i+1)*duration

                generated_store_capacity = StoreCapacity(
                    start=start_datetime,
                    end=end_datetime,
                    store=form.cleaned_data['store'],
                    total_capacity=form.cleaned_data['total_capacity'],
                    reserved_capacity=form.cleaned_data['reserved_capacity']
                )
                generated_store_capacity.save()
                print(generated_store_capacity)

            # redirect to a new URL:
            return HttpResponseRedirect("/")
    else:
        pass


    stores = Store.objects.all()
    context["stores"] = stores
    context["start_date"] = dt.datetime.now().strftime("%d.%m.%Y")
    context["start_time"] = "08:00"
    context["duration"] = 20
    context["amount"] = 1
    context["total_capacity"] = 10
    context["reserved_capacity"] = 3

    return render(request, 'batch_create_store_capacities.html', context)
