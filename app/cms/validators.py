from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _



def validate_booking_fits_in_store_capacity(store_capacity_id):
    from cms.models import StoreCapacity
    store_capacity = StoreCapacity.objects.filter(id=store_capacity_id).first()

    if store_capacity.available_capacity() < 1:
        raise ValidationError('StoreCapacity is not sufficient')