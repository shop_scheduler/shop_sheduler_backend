from django.contrib import admin
from django.db.models import F

from .models import StoreType, Country, Address, Store, StoreCapacity, Booking

@admin.register(StoreType)
class StoreTypeAdmin(admin.ModelAdmin):
    list_display = ("name", )


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ("name", )


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ("street", "housenumber", "zip_code", "city", "country",)


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ("name", "type", "description", "address", "lat", "long",)
    list_filter = ("type",)


@admin.register(StoreCapacity)
class StoreCapacityAdmin(admin.ModelAdmin):
    list_display = ("store", "start", "end", "total_capacity", "reserved_capacity", "available_capacity",)
    list_filter = ("store",)


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    list_display = ("user", "identifier", "store_capacity",)
    list_filter = ("store_capacity", "user" )
