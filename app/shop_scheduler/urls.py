"""shop_scheduler URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.urls import include, path
from rest_framework import routers
from cms import views

router = routers.DefaultRouter()
router.register(r'stores', views.StoreViewSet, basename="stores")
router.register(r'storecapacities', views.StoreCapacityViewSet)
router.register(r'bookings', views.BookingViewSet)
router.register(r'storetypes', views.StoreTypeViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),

    # api
    path('api/', include(router.urls)),
    path('api/placebooking/<int:storecapacityid>/', views.place_booking, name='placebooking'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('validatebooking/<identifier>/', views.validate_booking, name='validatebooking'),
    path('batch_create_store_capacities/', views.batch_generate_store_capacities, name='batch_generate_store_capacities'),

    # allauth and invitations
    path('accounts/', include('allauth.urls')),
    path('invitations/', include('invitations.urls', namespace='invitations')),

    # shop portal
    path('', include('shop_portal.urls'))

]
