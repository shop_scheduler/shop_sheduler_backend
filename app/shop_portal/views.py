from datetime import date

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.template import loader
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.views.generic import CreateView

from cms.models import Store, StoreType, StoreCapacity, Booking
from cms.serializers import BookingSerializer
from shop_portal.forms import ListFilterStores, ListFilterStoreCapacities, ListFilterBookings, DeleteBookingForm
from shop_portal.models import StoreAdministration


@login_required(login_url='account_login')
def stores_management(request):
    template = loader.get_template('stores.html')

    store_administrations = StoreAdministration.objects.filter(users=request.user)

    stores = []
    for admin in store_administrations:
        stores.append(admin.store)

    context = {
        'stores': stores,
    }

    return HttpResponse(template.render(context, request))

@login_required(login_url='account_login')
def stores(request):
    template = loader.get_template('stores.html')

    # If this is a POST request then process the Form data
    form = ListFilterStores(request.POST)
    context = {
        'form': form,
    }

    if request.method == 'POST':


        # Check if the form is valid:
        print(form.is_valid())
        if form.is_valid():
            context['zip_code'] = form.cleaned_data['zip_code']
            context['store_type'] = form.cleaned_data['store_type']

            stores = Store.objects.all().order_by("name")

            if form.cleaned_data['zip_code']:
                stores = stores.filter(address__zip_code__contains=form.cleaned_data['zip_code'])
            if form.cleaned_data['store_type']:
                stores = stores.filter(type=form.cleaned_data['store_type'])

            context["stores"] = stores

    else:
        stores = Store.objects.all()
        context["stores"] = stores

    context['store_types'] = StoreType.objects.all().order_by("name")

    return HttpResponse(template.render(context, request))

@login_required(login_url='account_login')
def store_detail(request, storeid):
    template = loader.get_template('store_detail.html')
    context = {}
    store = Store.objects.get(pk=storeid)
    context["store"] = store

    form = ListFilterStoreCapacities(request.POST)
    context["form"] = form

    if request.method == 'POST':

        # Check if the form is valid:
        print(form.is_valid())
        if form.is_valid():
            print(form.cleaned_data)
            context['date'] = form.cleaned_data['date']

            storecapacities = StoreCapacity.objects.filter(store=store).order_by('start')

            if form.cleaned_data['date']:
                storecapacities = storecapacities.filter(start__date=form.cleaned_data['date'])

            context["storecapacities"] = storecapacities

    else:
        storecapacities = StoreCapacity.objects.filter(store=store)
        context["storecapacities"] = storecapacities


    context["storecapacities"] = storecapacities

    return HttpResponse(template.render(context, request))

@login_required(login_url='account_login')
def place_booking(request, storecapacityid):
    template = loader.get_template('placebooking.html')
    context = {}

    storecapacity = StoreCapacity.objects.get(pk=storecapacityid)
    context["storecapacity"] = storecapacity
    context["method"] = request.method

    if request.method == 'POST':
        if storecapacity.available_capacity() > 0:
            if len(Booking.objects.filter(user=request.user, store_capacity=storecapacity)) == 0:
                booking = Booking(user=request.user, identifier=get_random_string(length=6, allowed_chars='1234567890'),
                                  store_capacity=storecapacity)
                booking.save()
                context["booking"] = BookingSerializer(booking).data
    else:
        pass

    return HttpResponse(template.render(context, request))

@login_required(login_url='account_login')
def own_bookings(request):
    template = loader.get_template("mybookings.html")
    context = {}

    today_bookings = Booking.objects.filter(user=request.user, store_capacity__start__date=date.today()).order_by("store_capacity__start")
    context["today_bookings"] = today_bookings

    form = ListFilterBookings(request.POST)
    context["form"] = form

    context["show_past_bookings"] = False



    if request.method == 'POST':

        # Check if the form is valid:
        print(form.is_valid())
        print(form.errors)
        if form.is_valid():
            print(form.cleaned_data)
            context['date'] = form.cleaned_data['date']
            context['show_past_bookings'] = form.cleaned_data['show_past_bookings']
            bookings = Booking.objects.filter(user=request.user).order_by("store_capacity__store",
                                                                          "store_capacity__start")
            if form.cleaned_data['date']:
                bookings = bookings.filter(store_capacity__start__date=form.cleaned_data['date'])
            else:
                if not context['show_past_bookings']: # if show past bookings is not checked, filter for future appointments
                    bookings = bookings.filter(store_capacity__start__date__gte=date.today())
            context["bookings"] = bookings


    else:
        if not context['show_past_bookings']:  # if show past bookings is not checked, filter for future appointments
            bookings = Booking.objects.filter(user=request.user).order_by("store_capacity__store",
                                                                          "store_capacity__start")
            bookings = bookings.filter(store_capacity__start__date__gte=date.today())
            context["bookings"] = bookings


        context["bookings"] = bookings

    print(context)

    return HttpResponse(template.render(context, request))

@login_required(login_url='account_login')
def booking_detail(request, bookingid):
    template = loader.get_template('booking_detail.html')
    context = {}

    booking = get_object_or_404(Booking, id=bookingid, user=request.user)
    context["booking"] = booking


    return HttpResponse(template.render(context, request))


def booking_delete(request, bookingid):
    template = loader.get_template('booking_delete.html')
    context = {}

    booking = get_object_or_404(Booking, id=bookingid, user=request.user)
    context["booking"] = booking


    if request.method == 'POST':
        form = DeleteBookingForm(request.POST, instance=booking)

        if form.is_valid(): # checks CSRF
            booking.delete()
            return HttpResponseRedirect(reverse("shop_portal_own_bookings")) # wherever to go after deleting

    else:
        form = DeleteBookingForm(instance=booking)
    context["form"] = form

    return HttpResponse(template.render(context, request))

def impressum(request):
    template = loader.get_template("impressum.html")
    context = {
        'impressum_name': settings.IMPRESSUM_NAME,
        'impressum_address': settings.IMPRESSUM_ADDRESS,
        'impressum_contact': settings.IMPRESSUM_CONTACT,
    }
    return HttpResponse(template.render(context, request))
