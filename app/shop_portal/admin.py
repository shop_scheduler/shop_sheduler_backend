from django.contrib import admin

# Register your models here.
from shop_portal.models import StoreAdministration


@admin.register(StoreAdministration)
class StoreAdministrationAdmin(admin.ModelAdmin):
    list_display = ("store", 'count_users')
    filter_horizontal = ['users',]
