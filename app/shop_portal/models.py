from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from cms.models import Store


class StoreAdministration(models.Model):
    # assigns users to stores
    users = models.ManyToManyField(User)
    store = models.OneToOneField(Store, on_delete=models.SET_NULL, null=True, blank=True)

    def count_users(self):

        return self.users.count()
    count_users.short_description = "# Users"