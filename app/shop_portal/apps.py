from django.apps import AppConfig


class ShopPortalConfig(AppConfig):
    name = 'shop_portal'
