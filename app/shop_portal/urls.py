from django.urls import path
from django.views.generic import TemplateView

from shop_portal import views

urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='shop_portal_home'),
    path('impressum/', views.impressum, name="impressum"),
    path('stores', views.stores, name="shop_portal_stores"),
    path('storedetail/<storeid>/', views.store_detail, name="shop_portal_store_detail"),
    path('placebooking/<storecapacityid>/', views.place_booking, name="shop_portal_place_booking"),
    path('mybookings/', views.own_bookings, name="shop_portal_own_bookings"),
    path('mybookings/<bookingid>/', views.booking_detail, name="shop_portal_booking_detail"),
    path('mybookings/<bookingid>/delete/', views.booking_delete, name="shop_portal_booking_delete"),
]