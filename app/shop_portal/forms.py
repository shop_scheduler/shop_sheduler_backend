from django import forms

from cms.models import StoreType, Booking


class ListFilterStores(forms.Form):
    zip_code = forms.CharField(required=False)
    store_type = forms.ModelChoiceField(queryset=StoreType.objects.all().order_by('name'), required=False, empty_label="any")


class ListFilterStoreCapacities(forms.Form):
    date = forms.DateField(required=False)


class ListFilterBookings(forms.Form):
    date = forms.DateField(required=False)
    show_past_bookings = forms.BooleanField(required=False)


class DeleteBookingForm(forms.ModelForm):
    class Meta:
        model = Booking
        fields = []
