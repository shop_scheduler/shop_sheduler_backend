set -e

python manage.py collectstatic --noinput

python manage.py migrate

gunicorn --bind=0.0.0.0 -w 4 shop_scheduler.wsgi
